# WOL Retransmit
**WOL Retransmit** is a utility to transform HTTP request into WOL magic packet.

# Installation
To install the utility go to the [Releases](https://gitlab.com/YaZasnyal/wol-retransmitter/-/releases) page and download sutable version of the package and install it. 

```sh
wget <package_url>
dpkg -i wolretransmit_<version>_<arch>.deb
```

# Usage
After installation process is complete the service will up and running on port ```25023```. To send WOL to the LAN HTTP POST request must be sent to the app with mac address of the device. 
```json
{
	"mac": "123456789abc"
}
```

Example:
```sh
curl -X POST -H "Content-Type: application/json" --data-raw '{"mac": "123456789abc"}' http://127.0.0.1:25023
```
