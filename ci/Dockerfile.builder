FROM rust:1.75.0

RUN apt-get update &&\ 
    apt-get -y install binutils-arm-linux-gnueabihf &&\ 
    apt-get clean autoclean
RUN rustup target add armv7-unknown-linux-musleabihf &&\
    rustup target add arm-unknown-linux-musleabihf &&\
    cargo install cargo-deb

WORKDIR /app
