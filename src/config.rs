use clap::Parser;

static DEFAULT_PORT: u16 = 25023;

/// Program to send broadcast WOL packets on HTTP request
#[derive(Default, Parser, Debug)]
#[clap(author, version, about, long_about = None)]
pub struct Args {
    /// Port for http requests
    #[clap(short, long, env = "WOLRETRANSMIT_PORT", default_value_t = DEFAULT_PORT)]
    pub port: u16,
}

pub fn parse_config() -> Args {
    let args = Args::parse();

    args
}
