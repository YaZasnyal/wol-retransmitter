mod config;

use warp::Filter;

mod filters {
    use super::models::Wol;
    use warp::Filter;

    pub fn json_body() -> impl Filter<Extract = (Wol,), Error = warp::Rejection> + Clone {
        warp::body::content_length_limit(1024 * 16).and(warp::body::json())
    }
}

mod models {
    use serde_derive::{Deserialize, Serialize};

    #[derive(Debug, Deserialize, Serialize, Clone)]
    pub struct Wol {
        pub mac: String,
    }
}

mod handlers {
    use super::models::Wol;
    use byteorder::{BigEndian, WriteBytesExt};
    use std::{convert::Infallible, net::SocketAddrV4};
    use tokio::net::UdpSocket;
    use warp::http::{Response, StatusCode};

    fn make_err(status: StatusCode, text: &str) -> warp::http::Result<Response<String>> {
        Response::builder().status(status).body(String::from(text))
    }

    pub async fn process_wol(wol: Wol) -> Result<impl warp::Reply, Infallible> {
        let Ok(val) = u64::from_str_radix(wol.mac.as_str(), 16) else {
            return Ok(make_err(
                StatusCode::BAD_REQUEST,
                format!("Unable to convert '{}' to uint", wol.mac).as_str(),
            ));
        };

        let mut buf = vec![];
        // preamble
        for _ in 0..6 {
            buf.write_u8(0xFF)
                .expect("unable to write to buf: probably wring size");
        }
        // main body
        for _ in 0..16 {
            buf.write_u48::<BigEndian>(val)
                .expect("unable to write to buf: probably wring size");
        }

        let socket = match UdpSocket::bind("0.0.0.0:0").await {
            Ok(x) => x,
            Err(_) => {
                return Ok(make_err(
                    StatusCode::INTERNAL_SERVER_ERROR,
                    "Unable to create server",
                ))
            }
        };
        socket
            .set_broadcast(true)
            .expect("unable to make socket broadcast");

        let interfaces = if_addrs::get_if_addrs().expect("unable to get network devices");

        for iface in interfaces {
            if iface.is_loopback() {
                continue;
            }
            let if_addrs::IfAddr::V4(ipv4) = iface.addr else {
                continue;
            };
            let Some(bcast) = ipv4.broadcast else {
                continue;
            };

            socket
                .send_to(&buf, SocketAddrV4::new(bcast, 9))
                .await
                .expect("unable to send message to this interface");
        }

        Ok(make_err(StatusCode::OK, ""))
    }
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let args = crate::config::parse_config();

    let wol = warp::path!()
        .and(warp::post())
        .and(filters::json_body())
        .and_then(handlers::process_wol);

    let (_addr, fut) =
        warp::serve(wol).bind_with_graceful_shutdown(([0, 0, 0, 0], args.port), async move {
            tokio::signal::ctrl_c()
                .await
                .expect("failed to listen to shutdown signal")
        });
    fut.await;
}
